<!doctype html>
<html lang="<?= get_locale() ?>">
<head>

    <!-- Page title -->
    <title><?= the_title() ?></title>

    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- WP Head -->
    <?php wp_head() ?>

</head>
<body>

    <header class="border-bottom pt-3 pb-3">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <b>CODING STYLE</b><span class="d-none d-lg-inline"> | DEMO Project</span>
                </div>
                <div class="col-6 text-end">
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'primary'
                        ));
                    ?>
                </div>
            </div>
        </div>
    </header>
    <section id="explainer" class="explainer">
        <div class="container">
            <div class="row">
                <div class="mb-4 mb-lg-0 col-12 col-lg-6 pe-md-5">
                    <p class="title">
                        WHAT AM I LOOKING AT?
                    </p>
                    <p class="text">
                        I will never share any source code with unauthorized people. That is great for my employers, but not so good for me, since I have nothing to show when I search for a new position. And that is exactly why I made this mini project - to have some source code showing my coding style without affecting the privacy & security of my past clients.
                    </p>
                </div>
                <div class="mb-4 mb-lg-0 col-12 col-lg-3 pe-md-5">
                    <p class="title">
                        HOW DO I ACCESS THE CODE?
                    </p>
                    <p class="text">
                        I made the code public, available on my Gilab account - <a href="https://gitlab.com/imaweb.sk/wp-acf-with-gutenberg" target="_blank">have a look here</a>.
                    </p>
                </div>
                <div class="col-12 col-lg-3">
                    <p class="title">
                        HOW DO I ACCESS WP ADMIN?
                    </p>
                    <p class="text">
                        Write me on LinkedIn and will be glad to provide the credentials - <a href="https://linkedin.com/in/martin-hlavacka" target="_blank">get in touch</a>.
                    </p>
                </div>
            </div>
        </div>
    </section>

