<section class="section-content-image">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img
                    src="<?=get_sub_field('image')['sizes']['medium_large']?>"
                    class="img-fluid"
                >
            </div>
            <div class="col-md-6 ps-md-5">
                <h3>
                    <?= get_sub_field('content')['title'] ?>
                </h3>
                <h2>
                    <?= get_sub_field('content')['subtitle'] ?>
                </h2>
                <div>
                    <?= get_sub_field('content')['description'] ?>
                </div>
            </div>
        </div>
    </div>
</section>