<?php
    $rand = rand();
    $containerize = get_sub_field('containerize_content');
    $background_color = get_sub_field('background_color');
    $padding_top = get_sub_field('padding_top');
    $padding_bottom = get_sub_field('padding_bottom');
    $slides = get_sub_field('images');
    $slides_to_scroll = get_sub_field('slides_to_scroll');
    $slides_to_show = get_sub_field('slides_to_show');
    $slides_height = get_sub_field('height_of_the_slides');
    $autoplay = get_sub_field('autoplay') ? "true" : "false";
    $autoplay_speed = get_sub_field('autoplay_speed') ? get_sub_field('autoplay_speed') : 2000;
?>

<section
    class="section-slider"
    style="
        background-color: <?= $background_color ?>;
        padding-top: <?= $padding_top ?>px;
        padding-bottom: <?= $padding_bottom ?>px;
    "
>
    <?php if($containerize) { ?>
        <div class="container">
    <?php } ?>
        <div class="slick-arrows-holder position-relative">
            <div class="slick-arrows overflow-auto">
                <div
                    class="slick-arrow slick-arrow-back slick-arrow-back-<?= $rand; ?> float-start"
                    style="margin-top:<?= ($slides_height / 2) - 25; ?>px;"
                >
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
                    </svg>
                </div>
                <div
                    class="slick-arrow slick-arrow-next slick-arrow-next-<?= $rand; ?> float-end"
                    style="margin-top:<?= ($slides_height / 2) - 25; ?>px;"
                >
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                    </svg>
                </div>
            </div>
        </div>
        <div
            class="slick"
            data-slick='{
                "slidesToShow": <?= $slides_to_show ?>,
                "slidesToScroll": <?= $slides_to_scroll ?>,
                "prevArrow": ".slick-arrow-back-<?= $rand; ?>",
                "nextArrow": ".slick-arrow-next-<?= $rand; ?>",
                "autoplay": <?= $autoplay ?>,
                "autoplaySpeed": <?= $autoplay_speed ?>,
                "lazyLoad": "ondemand",
                "responsive": [
                    {
                        "breakpoint": "1200",
                        "settings": {
                            "slidesToShow": 2,
                            "slidesToScroll": 1
                        }
                    },
                    {
                        "breakpoint": "992",
                        "settings": {
                            "slidesToShow": 1,
                            "slidesToScroll": 1
                        }
                    }
                ]
            }'
        >
            <?php foreach ($slides as $slide) { ?>
                <div
                    class="single-slide"
                    style="
                        background-image: url('<?= $slide['image']['sizes']['medium_large'] ?>');
                        height: <?= $slides_height; ?>px;
                    "
                >
                    <div class="slide-text">
                        <h2>
                            <?= $slide['title'] ?>
                        </h2>
                        <p>
                            <?= $slide['description'] ?>
                        </p>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php if($containerize) { ?>
        </div>
    <?php } ?>
</section>
