    <footer class="border-top pt-3 pb-3">
        <div class="container">
            Martin Hlavacka, Example of coding style, 2021-22
        </div>
    </footer>

    <!-- WP Footer -->
    <?php wp_footer(); ?>

</body>
</html>