<?php

    require __DIR__ . "/vendor/autoload.php";

    use ScssPhp\ScssPhp\Compiler;

    class Starter
    {

        public function __construct()
        {
            $this->enqueueStylesheetsAdmin();
            $this->enqueueStylesheetsPublic();
            $this->enqueuejQuery();
            $this->enqueueScriptsAdmin();
            $this->enqueueScriptsPublic();
            $this->registerPostTypes();
            $this->registerOptionsPage();
            $this->addThemeSupports();
            $this->registerMenus();
            $this->compileScss();
            $this->addCustomStylingToWYSIWYG();
            $this->removeEmojisFromWordpress();
            $this->allowUnfilteredSVGUploads();
        }

        private function registerPostTypes()
        {
            add_action( 'init', function(){
                register_post_type( 'employee', [
                    'public'    => true,
                    'label'     => 'Employees',
                    'menu_icon' => 'dashicons-businessperson',
                ]);
            });
        }

        private function enqueueStylesheetsAdmin()
        {
            add_action('admin_enqueue_scripts', function() {
                wp_enqueue_style('slick-slider', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
                wp_enqueue_style('theme-style', get_template_directory_uri() . '/css/admin.css');
            });
        }

        private function enqueueStylesheetsPublic()
        {
            add_action('wp_enqueue_scripts', function() {
                wp_enqueue_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css');
                wp_enqueue_style('slick-slider', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
                wp_enqueue_style('theme-style', get_template_directory_uri() . '/css/public.css');
            });
        }

        private function enqueuejQuery()
        {
            add_action('wp_enqueue_scripts', function(){
                wp_deregister_script('jquery');
                wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.6.0.min.js');
            });
        }

        private function enqueueScriptsAdmin()
        {
            add_action('admin_enqueue_scripts', function(){
                wp_enqueue_script('slick-slider', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', 'jquery');
                wp_enqueue_script('main', get_template_directory_uri() . '/js/admin.js');
            });
        }

        private function enqueueScriptsPublic()
        {
            add_action('wp_enqueue_scripts', function(){
                wp_enqueue_script('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js', 'jquery');
                wp_enqueue_script('slick-slider', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', 'jquery');
                wp_enqueue_script('main', get_template_directory_uri() . '/js/public.js');
            });
        }

        private function registerOptionsPage()
        {
            if(function_exists('acf_add_options_page')) {
                acf_add_options_page(array(
                    'page_title' 	=> 'Theme Settings',
                    'menu_title'	=> 'Theme Settings',
                    'menu_slug' 	=> 'theme-settings',
                    'capability'	=> 'edit_posts',
                    'redirect'		=> false
                ));
            }
        }

        private function addThemeSupports()
        {
            add_theme_support('menus');
            add_theme_support('post-thumbnails');
        }

        private function registerMenus()
        {
            add_action('init', function(){
                register_nav_menus(
                    array(
                        'primary' => __('Primary menu'),
                        'secondary' => __('Secondary menu')
                    )
                );
            });
        }

        private function compileScss()
        {
            if(is_admin() && get_field('compilation', 'options')) {
                foreach ([
                    'admin.scss' => 'admin.css',
                    'public.scss' => 'public.css'
                ] as $source => $destination) {
                    $compiler = new Compiler();
                    $compiled = $compiler->compileString(file_get_contents(__DIR__ . "/scss/" . $source))->getCss();
                    file_put_contents(__DIR__ . "/css/" . $destination, $compiled);
                }
            }
        }

        private function removeEmojisFromWordpress()
        {
            remove_action('wp_head', 'print_emoji_detection_script', 7);
            remove_action('admin_print_scripts', 'print_emoji_detection_script');
            remove_action('wp_print_styles', 'print_emoji_styles');
            remove_action('admin_print_styles', 'print_emoji_styles');
        }

        private function addCustomStylingToWYSIWYG()
        {
            add_filter('mce_buttons_2', function($buttons){
                array_unshift($buttons, 'styleselect');
                return $buttons;
            });
            add_filter('tiny_mce_before_init', function($init_array) {
                $style_formats = array(
                    array(
                        'title' => 'Blue Button',
                        'block' => 'span',
                        'classes' => 'blue-button',
                        'wrapper' => true,
                    ),
                    array(
                        'title' => 'Red Button',
                        'block' => 'span',
                        'classes' => 'red-button',
                        'wrapper' => true,
                    ),
                );
                $init_array['style_formats'] = json_encode( $style_formats );
                return $init_array;
            });
        }

        private function allowUnfilteredSVGUploads()
        {
            add_filter('upload_mimes', function($mimes){
                if(get_field('allow_unfiltered_svg_upload', 'options')) {
                    $mimes['svg'] = 'image/svg';
                }
                return $mimes;
            });
        }

    }

    if(defined('ABSPATH')){
        new Starter();
    }

?>