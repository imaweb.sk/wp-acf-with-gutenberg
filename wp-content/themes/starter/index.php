<?php

    /** Header */
    get_header();

?>

<!-- ACF Flexible Content -->
<main>
    <?php while (have_rows('flexible_content')) {
        the_row();
        include(__DIR__ . '/flexible-content/' . get_row_layout() . '.php');
    } ?>
</main>

<?php

    /** Footer */
    get_footer();

?>