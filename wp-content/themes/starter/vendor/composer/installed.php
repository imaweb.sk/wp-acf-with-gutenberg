<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'scssphp/scssphp' => array(
            'pretty_version' => 'v1.9.0',
            'version' => '1.9.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../scssphp/scssphp',
            'aliases' => array(),
            'reference' => 'bc8bece4e5e176973a832f3763049ddbba16e6fd',
            'dev_requirement' => false,
        ),
    ),
);
