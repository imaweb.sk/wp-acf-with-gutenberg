<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'C:\Projects\Starter\wp-content\plugins\wp-super-cache/' );
define( 'DB_NAME', 'starter' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'l|N?4Uf!AiPJY)nEu3h7e((G=B6<-Mc:4;Z(N>J/[>`ym5%S-PqpOxt|VXc M:Fg' );
define( 'SECURE_AUTH_KEY',  'xst|E2*_jGJP1pc~LMgJHY/(*c/e$P#t5e.BTYL!E43NvdNEtMFNFJm,c%paZ^G1' );
define( 'LOGGED_IN_KEY',    '=K}x|+&R#/m$/|4g5P:Id1Fp>AGgram4Yn#]f{Yz2X0=3RP-3~uTmr9F<g)5Rz$z' );
define( 'NONCE_KEY',        ');xC6=n#wbT?NKa&^8!I,`>[0_-fNWjb(@ZWmlw[-a8F]1>VS(~a[a(EA%g Y#h)' );
define( 'AUTH_SALT',        'A_o(GW>xA&?zZBO8_0.ImP<y/,woJ~OV1bZ={r$Or]Vo{~_KsAC>=?N{&>9W(MpR' );
define( 'SECURE_AUTH_SALT', '+EDz, aW}7dWLYq4Z|)Rsl<1edQtn@L>jt:)=Lnz(rSwNFDR,7@vfd0l3KEYc[0F' );
define( 'LOGGED_IN_SALT',   '11S%aQJgo`nRm21Qs3hD<oARP@GfnH(:~s9@EgO]j>[8)rCeG4bb;>,*X9xxB=FW' );
define( 'NONCE_SALT',       'K cpFm{)c5^H>T4tmF+1lk>L`^>J+TOFn9%Xad0{?^)zg/bw%w{%gRVUe:zle&#S' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
